#include <stdio.h>
#include <string.h>

int main (){

    char a[30] = {0};

    printf("Ingrese una palabra que contenga 10 letras (en mayusculas): ");
    scanf("%s", a);

    for (int i = 0; i < strlen(a); i++){
        a[i] = (a[i] + 3);
        if (a[i] + 3 > 90){
            a[i] = a[i] - 26;
        }
    }
    printf("\nPalabra encriptada: %s\n", a);
}
