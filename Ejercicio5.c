#include <stdio.h>

int main()
{
    char palabra[50];
    int i = 0;
    int VOCmay = 0;
    int VOCmin = 0;

    printf("\nIngrese una palabra: ");
    scanf("%s", &palabra);

    while(palabra[i] != '\0')
    {
        if(palabra[i] == 'a' || palabra[i] == 'e' || palabra[i] == 'i' || palabra[i] == 'o' || palabra[i] == 'u')
        {
            VOCmin++;
        }

        if(palabra[i] == 'A' || palabra[i] == 'E' || palabra[i] == 'I' || palabra[i] == 'O' || palabra[i] == 'U')
        {
            VOCmay++;
        }
        i++;
    }
    printf("\nLa palabra tiene %d vocales minusculas y %d vocales mayusculas\n", VOCmin, VOCmay);

}
