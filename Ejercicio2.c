#include <stdio.h>
#include <stdlib.h>

int main()
{
    char palabra[200];
    int i = 0;
    int j;

    printf("\nIngrese una palabra: ");
    gets(palabra);
    printf("\nLa palabra introducida es: %s\n\n", palabra);

    while(palabra[i]!='\0'){

        i++;
    }
    printf("La palabra al revez es: ");

    for (j=i-1; j>=0; j--){

        printf("%c", palabra[j]);
    }
    puts("");
    return 0;
}
