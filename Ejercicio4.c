#include <stdio.h>

int main ()
{
    char palabra[21];
    int letra = 0;
    int i = 0;

    printf("Ingrese una palabra: ");
    scanf("%s", &palabra);

    while(palabra[letra] != '\0')
    {
        if(palabra[letra] == 'a')
        {
            i++;
        }
        letra++;
    }
    printf("\nLa palabra tiene %d minusculas", i);
}
